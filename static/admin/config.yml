backend:
  name: gitlab
  repo: stefangeyer/big.tuwien.ac.at
  auth_type: implicit
  app_id: 4917865e19c3216d8102f0f164a191f3f53a7753151d459ef8365ec7548e71a5
  branch: master
media_folder: 'static/img/'
public_folder: 'img'
collections:
  - name: news
    label: News
    label_singular: News
    folder: 'content/news'
    path: "{{slug}}/index"
    create: true  # Allow users to create new documents in this collection
    fields:  # The fields each document in this collection have
      - {label: "Title", name: "title", widget: "string"}
      - {label: "Publish Date", name: "date", widget: "datetime"}
      - {label: "Subtitle", name: "subtitle", widget: "string", required: false}
      - {label: "Summary", name: "summary", widget: "markdown", required: false}
      - {label: "Draft", name: "draft", widget: "boolean", required: false, default: false}
      - {label: "Featured", name: "featured", widget: "boolean", required: false, default: false}
      - {label: "Authors", name: "authors", widget: "list", required: false}
      - {label: "Tags", name: "tags", widget: "list", required: false}
      - {label: "Categories", name: "categories", widget: "list", required: false}
      - {label: "Projects", name: "projects", widget: "list", required: false}
      - {label: "Body", name: "body", widget: "markdown"}
  - name: "people"
    label: "People"
    label_singular: "Person"
    folder: "content/people"
    create: false
    identifier_field: "name"
    path: "{{slug}}/_index"
    slug: "{{identifier}}"
    fields: 
      - {label: "Authors", name: "authors", widget: "hidden"}
      - {label: "Identifier", name: "identifier", widget: "hidden"}
      - {label: "Bio", name: "bio", widget: "hidden"}
      - {label: "Email", name: "email", widget: "hidden"}
      - {label: "Name", name: "name", widget: "hidden"}
      - {label: "Pairs", name: "pairs", widget: "hidden"}
      - {label: "Role", name: "role", widget: "hidden"}
      - {label: "Superuser", name: "superuser", widget: "hidden"}
      - {label: "User Groups", name: "user_groups", widget: "hidden"}
      - {label: "Body", name: "body", widget: "markdown", required: false, hint: "All other metadata is fetched from TISS periodically and can therefore not be altered here. Please adjust via TISS."}
  - name: "offered-topics"
    label: "Offered Topics"
    label_singular: "Offered Topic"
    folder: "content/offered-topic"
    path: "{{slug}}/index"
    create: true
    fields:
      - {label: "Title", name: "title", widget: "string"}
      - {label: "Subtitle", name: "subtitle", widget: "string", required: false}
      - {label: "Summary", name: "summary", widget: "markdown", required: false}
      - {label: "Authors", name: "authors", widget: "list", required: false}
      - {label: "Publish Date", name: "date", widget: "datetime"}
      - {label: "Draft", name: "draft", widget: "boolean", required: false, default: false}
      - {label: "Featured", name: "featured", widget: "boolean", required: false, default: false}
      - {label: "Categories", name: "categories", widget: "list", required: false}
      - {label: "Body", name: "body", widget: "markdown", required: false}
  - name: "master-theses"
    label: "Master Theses"
    label_singular: "Master Thesis"
    folder: "content/master-thesis"
    path: "{{slug}}/index"
    create: true
    fields:
      - {label: "Title", name: "title", widget: "string"}
      - {label: "Summary", name: "summary", widget: "markdown", required: false}
      - {label: "Publish Date", name: "date", widget: "datetime"}
      - {label: "Authors", name: "authors", widget: "list", required: false}
      - {label: "Advisors", name: "advisors", widget: "list", required: false}
      - {label: "Tags", name: "tags", widget: "list", hint: "Should include either 'Ongoing' or 'Finished' for sorting"}
      - {label: "Body", name: "body", widget: "markdown", required: false}
  - name: "phd-theses"
    label: "PHD Theses"
    label_singular: "PHD Thesis"
    folder: "content/phd-thesis"
    path: "{{slug}}/index"
    create: true
    fields:
      - {label: "Title", name: "title", widget: "string"}
      - {label: "Summary", name: "summary", widget: "markdown", required: false}
      - {label: "Publish Date", name: "date", widget: "datetime"}
      - {label: "Authors", name: "authors", widget: "list", required: false}
      - {label: "Advisors", name: "advisors", widget: "list", required: false}
      - {label: "Tags", name: "tags", widget: "list", hint: "Should include either 'Ongoing' or 'Finished' for sorting"}
      - {label: "Body", name: "body", widget: "markdown", required: false}
  - name: "projects"
    label: "Projects"
    label_singular: "Project"
    folder: "content/project"
    path: "{{slug}}/index"
    create: true
    fields:
      - {label: "Title", name: "title", widget: "string"}
      - {label: "Summary", name: "summary", widget: "markdown", required: false}
      - {label: "Publish Date", name: "date", widget: "datetime"}
      - {label: "Authors", name: "authors", widget: "list", required: false}
      - {label: "Tags", name: "tags", widget: "list", hint: "Should include either 'Ongoing' or 'Finished' for sorting"}
      - {label: "Body", name: "body", widget: "markdown"}
  - name: "seminars"
    label: "Seminars"
    label_singular: "Seminar"
    folder: "content/seminar"
    path: "{{slug}}/index"
    create: true
    fields:
      - {label: "Title", name: "title", widget: "string"}
      - {label: "Event", name: "event", widget: "string", required: false}
      - {label: "Event URL", name: "event_url", widget: "string", required: false, hint: "Link an external site"}
      - {label: "Location", name: "location", widget: "string", required: false}
      - {label: "Summary", name: "summary", widget: "string", required: false}
      - {label: "Begin Date", name: "date", widget: "datetime"}
      - {label: "End Date", name: "date_end", widget: "datetime", required: false}
      - {label: "Publish Date", name: "publishDate", widget: "datetime", hint: "Schedule page publish date"}
      - {label: "Authors", name: "authors", widget: "list", required: false}
      - {label: "Tags", name: "tags", widget: "list", hint: "Should include either 'Diploma Seminar' or 'Research Seminar' for sorting and ideally the semester"}
      - {label: "Featured", name: "featured", widget: "hidden", default: true}
      - {label: "Body", name: "body", widget: "markdown"}
  - name: "sites"
    label: "Sites"
    label_singular: "Site"
    files:
      - file: "content/home/home.md"
        label: "Home Segment"
        name: "home"
        fields:
          - {label: "Title", name: "title", widget: "string"}
          - {label: "Body", name: "body", widget: "markdown"}
      - file: "content/teaching/rooms.md"
        label: "Teaching: Rooms"
        name: "rooms"
        fields:
          - {label: "Title", name: "title", widget: "string"}
          - {label: "Body", name: "body", widget: "markdown"}
      - file: "content/teaching/info.md"
        label: "Teaching: Further Information"
        name: "info"
        fields:
          - {label: "Title", name: "title", widget: "string"}
          - {label: "Body", name: "body", widget: "markdown"}
      - file: "content/privacy.md"
        label: "Privacy Policy"
        name: "privacy"
        fields:
          - {label: "Title", name: "title", widget: "string"}
          - {label: "Publish Date", name: "date", widget: "datetime"}
          - {label: "Subtitle", name: "subtitle", widget: "string"}
          - {label: "Summary", name: "summary", widget: "markdown"}
          - {label: "Draft", name: "draft", widget: "boolean", required: false, default: false}
          - {label: "Body", name: "body", widget: "markdown"}
      - file: "content/terms.md"
        label: "Terms"
        name: "terms"
        fields:
          - {label: "Title", name: "title", widget: "string"}
          - {label: "Publish Date", name: "date", widget: "datetime"}
          - {label: "Subtitle", name: "subtitle", widget: "string"}
          - {label: "Summary", name: "summary", widget: "markdown"}
          - {label: "Draft", name: "draft", widget: "boolean", required: false, default: false}
          - {label: "Body", name: "body", widget: "markdown"}
