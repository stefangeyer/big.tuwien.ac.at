---
authors:
- christiane-floyd
bio: null
email: christiane.floyd@tuwien.ac.at
identitfier: christiane-floyd
name: Christiane Floyd
pairs:
- key: Mail
  link: mailto:christiane.floyd@tuwien.ac.at
  value: christiane.floyd@tuwien.ac.at
role: Hon.Prof.in Dr.in phil.
superuser: false
user_groups:
- Researchers
---

several times guest professor at BIG,

see „[Prof. em. Dr. Christiane Floyd erhält Ehrendoktorwürde der Universität Paderborn](https://www.min.uni-hamburg.de/ueber-die-fakultaet/aktuelles/2017-10-30.html)“