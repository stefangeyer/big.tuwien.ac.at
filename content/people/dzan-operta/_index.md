---
authors:
- dzan-operta
bio: null
email: dzan.operta@tuwien.ac.at
identitfier: dzan-operta
name: Dzan Operta
pairs:
- key: Mail
  link: mailto:dzan.operta@tuwien.ac.at
  value: dzan.operta@tuwien.ac.at
- key: Location
  value: HG0205
role: null
superuser: false
user_groups:
- Visitors and Friends
---