---
authors:
- radek-sindelar
bio: null
email: radek.sindelar@tuwien.ac.at
identitfier: radek-sindelar
name: Radek Sindelar
pairs:
- key: Mail
  link: mailto:radek.sindelar@tuwien.ac.at
  value: radek.sindelar@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---