---
authors:
- franziska-tiefenthaller
bio: null
email: franziska.tiefenthaller@tuwien.ac.at
identitfier: franziska-tiefenthaller
name: Franziska Tiefenthaller
pairs:
- key: Mail
  link: mailto:franziska.tiefenthaller@tuwien.ac.at
  value: franziska.tiefenthaller@tuwien.ac.at
- key: Location
  value: HC0211
role: Mag.a rer.nat.
superuser: false
user_groups:
- Visitors and Friends
---