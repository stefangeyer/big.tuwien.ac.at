---
authors:
- irfan-kacapor
bio: null
email: irfan.kacapor@tuwien.ac.at
identitfier: irfan-kacapor
name: Irfan Kacapor
pairs:
- key: Mail
  link: mailto:irfan.kacapor@tuwien.ac.at
  value: irfan.kacapor@tuwien.ac.at
- key: Location
  value: FB0106
role: null
superuser: false
user_groups:
- Visitors and Friends
---