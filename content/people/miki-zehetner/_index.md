---
authors:
- miki-zehetner
bio: null
email: miki.zehetner@tuwien.ac.at
identitfier: miki-zehetner
name: Miki Zehetner
pairs:
- key: Mail
  link: mailto:miki.zehetner@tuwien.ac.at
  value: miki.zehetner@tuwien.ac.at
- key: Location
  value: HG0210
role: Univ.Ass. DI
superuser: false
user_groups:
- Visitors and Friends
---