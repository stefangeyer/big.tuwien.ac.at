---
authors:
- dominik-bork
bio: null
email: dominik.bork@tuwien.ac.at
identitfier: dominik-bork
name: Dominik Bork
pairs:
- key: Mail
  link: mailto:dominik.bork@tuwien.ac.at
  value: dominik.bork@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 194308
  value: +43 1 58801 194308
- key: Location
  value: FB0116
role: Associate Prof. Dipl.-Wirtsch.Inf.Univ. Dr.rer.pol.
superuser: false
user_groups:
- Professors
---