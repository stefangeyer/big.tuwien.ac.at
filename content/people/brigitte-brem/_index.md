---
authors:
- brigitte-brem
bio: null
email: brigitte.brem@tuwien.ac.at
identitfier: brigitte-brem
name: Brigitte Brem
pairs:
- key: Mail
  link: mailto:brigitte.brem@tuwien.ac.at
  value: brigitte.brem@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 183405
  value: +43 1 58801 183405
role: Projektass.in Dr.in rer.nat. Mag.a rer.nat.
superuser: false
user_groups:
- Visitors and Friends
---