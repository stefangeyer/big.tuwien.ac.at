---
authors:
- karin-kappel
bio: null
email: karin.kappel@tuwien.ac.at
identitfier: karin-kappel
name: Karin Kappel
pairs:
- key: Mail
  link: mailto:karin.kappel@tuwien.ac.at
  value: karin.kappel@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 183408
  value: +43 1 58801 183408
role: Projektass.in Dipl.-Ing.in Mag.a rer.soc.oec. Dr.in techn.
superuser: false
user_groups:
- Visitors and Friends
---