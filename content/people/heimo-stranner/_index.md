---
authors:
- heimo-stranner
bio: null
email: heimo.stranner@tuwien.ac.at
identitfier: heimo-stranner
name: Heimo Stranner
pairs:
- key: Mail
  link: mailto:heimo.stranner@tuwien.ac.at
  value: heimo.stranner@tuwien.ac.at
role: Projektass. Dipl.-Ing.
superuser: false
user_groups:
- Visitors and Friends
---