---
authors:
- andreas-ehringfeld
bio: null
email: andreas.ehringfeld@tuwien.ac.at
identitfier: andreas-ehringfeld
name: Andreas Ehringfeld
pairs:
- key: Mail
  link: mailto:andreas.ehringfeld@tuwien.ac.at
  value: andreas.ehringfeld@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 183417
  value: +43 1 58801 183417
role: Projektass. Dipl.-Ing. Dr.techn.
superuser: false
user_groups:
- Visitors and Friends
---