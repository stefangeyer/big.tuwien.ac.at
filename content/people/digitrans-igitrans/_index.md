---
authors:
- digitrans-igitrans
bio: null
email: digitrans.e194-03.igitrans@tuwien.ac.at
identitfier: digitrans-igitrans
name: Digitrans Igitrans
pairs:
- key: Mail
  link: mailto:digitrans.e194-03.igitrans@tuwien.ac.at
  value: digitrans.e194-03.igitrans@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---