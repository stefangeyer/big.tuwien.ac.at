---
authors:
- andjela-djelic
bio: null
email: andjela.djelic@tuwien.ac.at
identitfier: andjela-djelic
name: Andjela Djelic
pairs:
- key: Mail
  link: mailto:andjela.djelic@tuwien.ac.at
  value: andjela.djelic@tuwien.ac.at
- key: Location
  value: FB0106
role: null
superuser: false
user_groups:
- Visitors and Friends
---