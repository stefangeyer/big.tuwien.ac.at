---
authors:
- simon-raedler
bio: null
email: simon.raedler@tuwien.ac.at
identitfier: simon-raedler
name: Simon Rädler
pairs:
- key: Mail
  link: mailto:simon.raedler@tuwien.ac.at
  value: simon.raedler@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---