---
authors:
- christian-schanes
bio: null
email: christian.schanes@tuwien.ac.at
identitfier: christian-schanes
name: Christian Schanes
pairs:
- key: Mail
  link: mailto:christian.schanes@tuwien.ac.at
  value: christian.schanes@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 183411
  value: +43 1 58801 183411
role: Projektass. Dipl.-Ing. Dr.techn.
superuser: false
user_groups:
- Visitors and Friends
---