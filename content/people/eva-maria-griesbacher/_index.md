---
authors:
- eva-maria-griesbacher
bio: null
email: eva-maria.griesbacher@tuwien.ac.at
identitfier: eva-maria-griesbacher
name: Eva-Maria Griesbacher
pairs:
- key: Mail
  link: mailto:eva-maria.griesbacher@tuwien.ac.at
  value: eva-maria.griesbacher@tuwien.ac.at
role: Projektass.in
superuser: false
user_groups:
- Visitors and Friends
---