---
authors:
- markus-gerhard-tauber
bio: null
email: markus.tauber@tuwien.ac.at
identitfier: markus-gerhard-tauber
name: Markus Gerhard Tauber
pairs:
- key: Mail
  link: mailto:markus.tauber@tuwien.ac.at
  value: markus.tauber@tuwien.ac.at
role: Dr.
superuser: false
user_groups:
- Visitors and Friends
---