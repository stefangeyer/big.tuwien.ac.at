---
authors:
- felix-winterleitner
bio: null
email: felix.winterleitner@tuwien.ac.at
identitfier: felix-winterleitner
name: Felix Winterleitner
pairs:
- key: Mail
  link: mailto:felix.winterleitner@tuwien.ac.at
  value: felix.winterleitner@tuwien.ac.at
- key: Location
  value: HG0205
role: null
superuser: false
user_groups:
- Visitors and Friends
---