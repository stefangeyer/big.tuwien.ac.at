---
authors:
- konstantinos-anagnostou
bio: null
email: konstantinos.anagnostou@tuwien.ac.at
identitfier: konstantinos-anagnostou
name: Konstantinos Anagnostou
pairs:
- key: Mail
  link: mailto:konstantinos.anagnostou@tuwien.ac.at
  value: konstantinos.anagnostou@tuwien.ac.at
role: Projektass. Dipl.-Ing.
superuser: false
user_groups:
- Visitors and Friends
---