---
authors:
- monika-malinova-mandelburger
bio: null
email: monika.mandelburger@tuwien.ac.at
identitfier: monika-malinova-mandelburger
name: Monika Malinova Mandelburger
pairs:
- key: Mail
  link: mailto:monika.mandelburger@tuwien.ac.at
  value: monika.mandelburger@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 194313
  value: +43 1 58801 194313
role: Dr.in rer.soc.oec.
superuser: false
user_groups:
- Visitors and Friends
---