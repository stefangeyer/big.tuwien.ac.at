---
authors:
- alena-melic
bio: null
email: alena.melic@tuwien.ac.at
identitfier: alena-melic
name: Alena Melic
pairs:
- key: Mail
  link: mailto:alena.melic@tuwien.ac.at
  value: alena.melic@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 740038
  value: +43 1 58801 740038
role: null
superuser: false
user_groups:
- Visitors and Friends
---