---
authors:
- alexandra-mazak-huemer
bio: null
email: mazak@big.tuwien.ac.at
identitfier: alexandra-mazak-huemer
name: Alexandra Mazak-Huemer
pairs:
- key: Mail
  link: mailto:mazak@big.tuwien.ac.at
  value: mazak@big.tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 188696
  value: +43 1 58801 188696
- key: Location
  value: HG0205
role: Dipl.-Ing.in Dr.in techn. Mag.a rer.soc.oec.
superuser: false
user_groups:
- Visitors and Friends
---