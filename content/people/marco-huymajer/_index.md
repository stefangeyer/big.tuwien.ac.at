---
authors:
- marco-huymajer
bio: null
email: huymajer@big.tuwien.ac.at
identitfier: marco-huymajer
name: Marco Huymajer
pairs:
- key: Mail
  link: mailto:huymajer@big.tuwien.ac.at
  value: huymajer@big.tuwien.ac.at
- key: Location
  value: FB0102
role: Projektass. Dipl.-Ing.
superuser: false
user_groups:
- Visitors and Friends
---

Marco Huymajer received a bachelor’s degree in Electrical Engineering and Information Technology and a master’s degree in Microelectronics and Photonics from the Technische Universität Wien in 2015 and 2016 respectively. He joined the Business Informatics Group in the course of a cooperation with the Institute of Interdisciplinary Construction Process Management. His current research topic is the application of model-driven software development and data science techniques to construction processes.