---
authors:
- philipp-lorenz-glaser
bio: null
email: philipp-lorenz.glaser@tuwien.ac.at
identitfier: philipp-lorenz-glaser
name: Philipp-Lorenz Glaser
pairs:
- key: Mail
  link: mailto:philipp-lorenz.glaser@tuwien.ac.at
  value: philipp-lorenz.glaser@tuwien.ac.at
- key: Location
  value: FB0103
role: null
superuser: false
user_groups:
- Visitors and Friends
---