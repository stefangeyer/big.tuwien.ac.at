---
authors:
- florian-kleedorfer
bio: null
email: florian.kleedorfer@tuwien.ac.at
identitfier: florian-kleedorfer
name: Florian Kleedorfer
pairs:
- key: Mail
  link: mailto:florian.kleedorfer@tuwien.ac.at
  value: florian.kleedorfer@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---