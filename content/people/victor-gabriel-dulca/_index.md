---
authors:
- victor-gabriel-dulca
bio: null
email: victor.dulca@tuwien.ac.at
identitfier: victor-gabriel-dulca
name: Victor Gabriel Dulca
pairs:
- key: Mail
  link: mailto:victor.dulca@tuwien.ac.at
  value: victor.dulca@tuwien.ac.at
role: Projektass. Dipl.-Ing.
superuser: false
user_groups:
- Researchers
---