---
authors:
- janis-wolf
bio: null
email: janis.wolf@tuwien.ac.at
identitfier: janis-wolf
name: Janis Wolf
pairs:
- key: Mail
  link: mailto:janis.wolf@tuwien.ac.at
  value: janis.wolf@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---