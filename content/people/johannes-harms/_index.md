---
authors:
- johannes-harms
bio: null
email: johannes.harms@tuwien.ac.at
identitfier: johannes-harms
name: Johannes Harms
pairs:
- key: Mail
  link: mailto:johannes.harms@tuwien.ac.at
  value: johannes.harms@tuwien.ac.at
role: Projektass. Dipl.-Ing. Dr.techn.
superuser: false
user_groups:
- Visitors and Friends
---