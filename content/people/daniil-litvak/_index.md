---
authors:
- daniil-litvak
bio: null
email: daniil.litvak@tuwien.ac.at
identitfier: daniil-litvak
name: Daniil Litvak
pairs:
- key: Mail
  link: mailto:daniil.litvak@tuwien.ac.at
  value: daniil.litvak@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---