---
authors:
- florian-fankhauser
bio: null
email: florian.fankhauser@tuwien.ac.at
identitfier: florian-fankhauser
name: Florian Fankhauser
pairs:
- key: Mail
  link: mailto:florian.fankhauser@tuwien.ac.at
  value: florian.fankhauser@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 183410
  value: +43 1 58801 183410
role: Projektass. Dipl.-Ing.
superuser: false
user_groups:
- Visitors and Friends
---