---
authors:
- nicholas-arthur-bzowski
bio: null
email: nicholas.bzowski@tuwien.ac.at
identitfier: nicholas-arthur-bzowski
name: Nicholas Arthur Bzowski
pairs:
- key: Mail
  link: mailto:nicholas.bzowski@tuwien.ac.at
  value: nicholas.bzowski@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---