---
authors:
- aleksandar-gavric
bio: null
email: aleksandar.gavric@tuwien.ac.at
identitfier: aleksandar-gavric
name: Aleksandar Gavric
pairs:
- key: Mail
  link: mailto:aleksandar.gavric@tuwien.ac.at
  value: aleksandar.gavric@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 194311
  value: +43 1 58801 194311
- key: Location
  value: FB0103
role: Univ.Ass.
superuser: false
user_groups:
- Visitors and Friends
---