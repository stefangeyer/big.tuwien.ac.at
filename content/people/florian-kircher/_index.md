---
authors:
- florian-kircher
bio: null
email: florian.kircher@tuwien.ac.at
identitfier: florian-kircher
name: Florian Kircher
pairs:
- key: Mail
  link: mailto:florian.kircher@tuwien.ac.at
  value: florian.kircher@tuwien.ac.at
- key: Location
  value: FB0106
role: null
superuser: false
user_groups:
- Visitors and Friends
---