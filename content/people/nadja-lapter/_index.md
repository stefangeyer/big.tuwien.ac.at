---
authors:
- nadja-lapter
bio: null
email: nadja.lapter@tuwien.ac.at
identitfier: nadja-lapter
name: Nadja Lapter
pairs:
- key: Mail
  link: mailto:nadja.lapter@tuwien.ac.at
  value: nadja.lapter@tuwien.ac.at
- key: Location
  value: FB0106
role: null
superuser: false
user_groups:
- Visitors and Friends
---