---
authors:
- claudia-habersack
bio: null
email: claudia.habersack@tuwien.ac.at
identitfier: claudia-habersack
name: Claudia Habersack
pairs:
- key: Mail
  link: mailto:claudia.habersack@tuwien.ac.at
  value: claudia.habersack@tuwien.ac.at
- key: Location
  value: HC0211
role: Mag.rer.nat.
superuser: false
user_groups:
- Organisation
---