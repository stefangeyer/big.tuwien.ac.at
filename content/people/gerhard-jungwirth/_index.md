---
authors:
- gerhard-jungwirth
bio: null
email: gerhard.jungwirth@tuwien.ac.at
identitfier: gerhard-jungwirth
name: Gerhard Jungwirth
pairs:
- key: Mail
  link: mailto:gerhard.jungwirth@tuwien.ac.at
  value: gerhard.jungwirth@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---