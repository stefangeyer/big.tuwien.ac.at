---
authors:
- mina-jafari
bio: null
email: mina.jafari@tuwien.ac.at
identitfier: mina-jafari
name: Mina Jafari
pairs:
- key: Mail
  link: mailto:mina.jafari@tuwien.ac.at
  value: mina.jafari@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 194310
  value: +43 1 58801 194310
role: null
superuser: false
user_groups:
- Visitors and Friends
---