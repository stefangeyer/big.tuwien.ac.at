---
authors:
- alexander-soutsos
bio: null
email: alexander.soutsos@tuwien.ac.at
identitfier: alexander-soutsos
name: Alexander Soutsos
pairs:
- key: Mail
  link: mailto:alexander.soutsos@tuwien.ac.at
  value: alexander.soutsos@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---