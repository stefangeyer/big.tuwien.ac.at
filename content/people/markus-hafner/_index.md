---
authors:
- markus-hafner
bio: null
email: markus.e194-03.hafner@tuwien.ac.at
identitfier: markus-hafner
name: Markus Hafner
pairs:
- key: Mail
  link: mailto:markus.e194-03.hafner@tuwien.ac.at
  value: markus.e194-03.hafner@tuwien.ac.at
- key: Location
  value: HG0215
role: null
superuser: false
user_groups:
- Visitors and Friends
---