---
authors:
- ari-marhali
bio: null
email: ari.marhali@tuwien.ac.at
identitfier: ari-marhali
name: Ari Marhali
pairs:
- key: Mail
  link: mailto:ari.marhali@tuwien.ac.at
  value: ari.marhali@tuwien.ac.at
- key: Location
  value: FB0106
role: null
superuser: false
user_groups:
- Visitors and Friends
---