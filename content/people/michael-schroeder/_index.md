---
authors:
- michael-schroeder
bio: null
email: michael.schroeder@tuwien.ac.at
identitfier: michael-schroeder
name: Michael Schröder
pairs:
- key: Mail
  link: mailto:michael.schroeder@tuwien.ac.at
  value: michael.schroeder@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 194306
  value: +43 1 58801 194306
role: Projektass. Dipl.-Ing.
superuser: false
user_groups:
- Researchers
---

Michael Schröder is a PhD student advised by {{% mention "juergen-cito" %}}.