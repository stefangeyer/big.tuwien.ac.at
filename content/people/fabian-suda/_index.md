---
authors:
- fabian-suda
bio: null
email: fabian.suda@tuwien.ac.at
identitfier: fabian-suda
name: Fabian Suda
pairs:
- key: Mail
  link: mailto:fabian.suda@tuwien.ac.at
  value: fabian.suda@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---