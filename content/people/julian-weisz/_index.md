---
authors:
- julian-weisz
bio: null
email: julian.weiss@tuwien.ac.at
identitfier: julian-weisz
name: Julian Weiß
pairs:
- key: Mail
  link: mailto:julian.weiss@tuwien.ac.at
  value: julian.weiss@tuwien.ac.at
- key: Location
  value: HG0211
role: null
superuser: false
user_groups:
- Visitors and Friends
---