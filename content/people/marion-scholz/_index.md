---
authors:
- marion-scholz
bio: null
email: marion.scholz@tuwien.ac.at
identitfier: marion-scholz
name: Marion Scholz
pairs:
- key: Mail
  link: mailto:marion.scholz@tuwien.ac.at
  value: marion.scholz@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 18842
  value: +43 1 58801 18842
- key: Location
  value: FB0114
role: Senior Lecturer Dipl.-Ing.in Mag.a rer.soc.oec.
superuser: false
user_groups:
- Researchers
---