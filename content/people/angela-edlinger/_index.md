---
authors:
- angela-edlinger
bio: null
email: angela.edlinger@tuwien.ac.at
identitfier: angela-edlinger
name: Angela Edlinger
pairs:
- key: Mail
  link: mailto:angela.edlinger@tuwien.ac.at
  value: angela.edlinger@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 194301
  value: +43 1 58801 194301
- key: Location
  value: FB0111
role: null
superuser: false
user_groups:
- Visitors and Friends
---