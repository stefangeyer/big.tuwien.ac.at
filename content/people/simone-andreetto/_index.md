---
authors:
- simone-andreetto
bio: null
email: simone.andreetto@tuwien.ac.at
identitfier: simone-andreetto
name: Simone Andreetto
pairs:
- key: Mail
  link: mailto:simone.andreetto@tuwien.ac.at
  value: simone.andreetto@tuwien.ac.at
role: Dipl.-Ing.
superuser: false
user_groups:
- Visitors and Friends
---