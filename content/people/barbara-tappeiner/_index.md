---
authors:
- barbara-tappeiner
bio: null
email: barbara.tappeiner@tuwien.ac.at
identitfier: barbara-tappeiner
name: Barbara Tappeiner
pairs:
- key: Mail
  link: mailto:barbara.tappeiner@tuwien.ac.at
  value: barbara.tappeiner@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 183402
  value: +43 1 58801 183402
role: Projektass.in Dipl.-Ing.in Dr.in techn.
superuser: false
user_groups:
- Visitors and Friends
---