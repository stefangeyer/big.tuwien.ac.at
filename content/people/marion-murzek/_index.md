---
authors:
- marion-murzek
bio: null
email: marion.murzek@tuwien.ac.at
identitfier: marion-murzek
name: Marion Murzek
pairs:
- key: Mail
  link: mailto:marion.murzek@tuwien.ac.at
  value: marion.murzek@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 188763
  value: +43 1 58801 188763
- key: Location
  value: FB0114
role: Senior Lecturer Mag.a rer.soc.oec. Dr.in rer.soc.oec.
superuser: false
user_groups:
- Researchers
---