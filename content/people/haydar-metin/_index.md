---
authors:
- haydar-metin
bio: null
email: haydar.metin@tuwien.ac.at
identitfier: haydar-metin
name: Haydar Metin
pairs:
- key: Mail
  link: mailto:haydar.metin@tuwien.ac.at
  value: haydar.metin@tuwien.ac.at
role: Univ.Lektor Dipl.-Ing.
superuser: false
user_groups:
- Visitors and Friends
---