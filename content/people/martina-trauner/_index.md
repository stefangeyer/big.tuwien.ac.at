---
authors:
- martina-trauner
bio: null
email: martina.trauner@tuwien.ac.at
identitfier: martina-trauner
name: Martina Trauner
pairs:
- key: Mail
  link: mailto:martina.trauner@tuwien.ac.at
  value: martina.trauner@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 194410
  value: +43 1 58801 194410
- key: Location
  value: HG0405
role: null
superuser: false
user_groups:
- Visitors and Friends
---