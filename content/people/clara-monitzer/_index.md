---
authors:
- clara-monitzer
bio: null
email: clara.monitzer@tuwien.ac.at
identitfier: clara-monitzer
name: Clara Monitzer
pairs:
- key: Mail
  link: mailto:clara.monitzer@tuwien.ac.at
  value: clara.monitzer@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 194301
  value: +43 1 58801 194301
- key: Location
  value: HC0211
role: null
superuser: false
user_groups:
- Visitors and Friends
---