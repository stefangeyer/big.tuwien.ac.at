---
authors:
- syed-juned-ali
bio: null
email: syed.juned.ali@tuwien.ac.at
identitfier: syed-juned-ali
name: Syed Juned Ali
pairs:
- key: Mail
  link: mailto:syed.juned.ali@tuwien.ac.at
  value: syed.juned.ali@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 194309
  value: +43 1 58801 194309
- key: Location
  value: FB0103
role: Univ.Ass.
superuser: false
user_groups:
- Visitors and Friends
---