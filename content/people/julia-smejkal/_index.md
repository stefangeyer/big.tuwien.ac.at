---
authors:
- julia-smejkal
bio: null
email: julia.smejkal@tuwien.ac.at
identitfier: julia-smejkal
name: Julia Smejkal
pairs:
- key: Mail
  link: mailto:julia.smejkal@tuwien.ac.at
  value: julia.smejkal@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---