---
authors:
- sebastian-kraehsmaier
bio: null
email: sebastian.kraehsmaier@tuwien.ac.at
identitfier: sebastian-kraehsmaier
name: Sebastian Krähsmaier
pairs:
- key: Mail
  link: mailto:sebastian.kraehsmaier@tuwien.ac.at
  value: sebastian.kraehsmaier@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---