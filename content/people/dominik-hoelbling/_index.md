---
authors:
- dominik-hoelbling
bio: null
email: dominik.hoelbling@tuwien.ac.at
identitfier: dominik-hoelbling
name: Dominik Hölbling
pairs:
- key: Mail
  link: mailto:dominik.hoelbling@tuwien.ac.at
  value: dominik.hoelbling@tuwien.ac.at
role: Projektass. Dr.rer.nat.
superuser: false
user_groups:
- Visitors and Friends
---