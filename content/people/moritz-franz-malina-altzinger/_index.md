---
authors:
- moritz-franz-malina-altzinger
bio: null
email: moritz.malina-altzinger@tuwien.ac.at
identitfier: moritz-franz-malina-altzinger
name: Moritz Franz Malina-Altzinger
pairs:
- key: Mail
  link: mailto:moritz.malina-altzinger@tuwien.ac.at
  value: moritz.malina-altzinger@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---