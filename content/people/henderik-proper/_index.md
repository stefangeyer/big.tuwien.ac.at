---
authors:
- henderik-proper
bio: null
email: henderik.proper@tuwien.ac.at
identitfier: henderik-proper
name: Henderik Proper
pairs:
- key: Mail
  link: mailto:henderik.proper@tuwien.ac.at
  value: henderik.proper@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 194303
  value: +43 1 58801 194303
- key: Location
  value: FB0101
role: Univ.Prof.
superuser: false
user_groups:
- Visitors and Friends
---