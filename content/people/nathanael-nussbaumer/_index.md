---
authors:
- nathanael-nussbaumer
bio: null
email: nathanael.nussbaumer@tuwien.ac.at
identitfier: nathanael-nussbaumer
name: Nathanael Nussbaumer
pairs:
- key: Mail
  link: mailto:nathanael.nussbaumer@tuwien.ac.at
  value: nathanael.nussbaumer@tuwien.ac.at
role: null
superuser: false
user_groups:
- Visitors and Friends
---