---
authors:
- marianne-schnellmann
bio: null
email: marianne.schnellmann@tuwien.ac.at
identitfier: marianne-schnellmann
name: Marianne Schnellmann
pairs:
- key: Mail
  link: mailto:marianne.schnellmann@tuwien.ac.at
  value: marianne.schnellmann@tuwien.ac.at
- key: Phone
  link: tel:+43 1 58801 194312
  value: +43 1 58801 194312
- key: Location
  value: FB0107
role: Univ.Ass.in
superuser: false
user_groups:
- Visitors and Friends
---