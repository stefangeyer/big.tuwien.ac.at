---
title: Entwicklung von Datenzugriffsmechanismen für CAD
summary: Building Information Modeling (BIM) has a twofold meaning. On the one
  hand, it describes the exchange of geometry data enriched by meta-information.
  On the other hand, it stands for the entire process of interdisciplinary
  planning of a building – with the simultaneous engagement of all stakeholders
  in the development of a common model containing all relevant information.
authors:
  - Gertrude Kappel
  - Galina Paskaleva
date: 2020-04-25T10:08:37.595Z
draft: false
featured: false
tags:
  - CAD
---
Für folgende Studienrichtungen:

* 066 443 Masterstudium Architektur,
* 066 444 Masterstudium Building Science and Technology,
* 066 505 Masterstudium Bauingenieurwissenschaften,
* 066 937 Masterstudium Software Engineering & Internet Computing,
* 066 932 Masterstudium Visual Computing,
* 066 935 Masterstudium Media and Human-Centered Computing,
* 066 931 Masterstudium Logik and Computation
* 066 936 Masterstudium Medizinische Informatik

Angebotene Sprachen: Deutsch oder Englisch

**Das Thema wird als Projektarbeit mit Option zur Durchführung einer Diplomarbeit vergeben!**

### Beschreibung DE
Unter Building Information Modeling (BIM) versteht man einerseits den Austausch von geometrischen Daten, die mit 
Meta-Information angereichert wurden, und andererseits das gleichzeitige Bearbeiten des gleichen Gebäudemodels 
durch alle beteiligten Fachrichtungen – von Architektur bis zur Gebäudeautomatisierung.
Diverse Computer Aided Design (CAD) Programme implementieren Teile von BIM. Ziel dieser Projektarbeit ist, ein 
bekanntes CAD Tool auf BIM-Tauglichkeit zu untersuchen, indem klar vordefinierte Tests über die zur Verfügung 
stehenden Application Programming Interface (API) durchgeführt werden.
Der oder die Masterstudent_In sollte Erfahrung mit CAD-Tools haben – Zeichnen von Vorentwurfsplänen, Bemaßung, 
Erstellung von dynamischen Bibliothekselementen, Plandruck und Datenaustausch zwischen CAD-Tools. Weiter ist 
Erfahrung mit dem Entwickeln von Plug-Ins für CAD-Tools notwendig, und mindestens Grundkenntnisse in mehreren der 
dazu verwendeten Programmiersprachen – z.B. Visual Basic for Applications (VBA) und C#.

**The topic is assigned as project work with an option to write a diploma thesis!**

### Beschreibung EN
Building Information Modeling (BIM) has a twofold meaning. On the one hand, it describes the exchange of geometry 
data enriched by meta-information. On the other hand, it stands for the entire process of interdisciplinary planning 
of a building – with the simultaneous engagement of all stakeholders in the development of a common model containing 
all relevant information.
Multiple Computer Aided Design (CAD) tools implement parts of BIM. The goal of this project is to evaluate the 
BIM-fitness of a well-known CAD tool by performing pre-defined tests. Those will assess its BIM-functionality via 
the available Application Programming Interface (API).
The master student should have experience in using CAD-Tools – drawing of building plans during the design phase, 
dimensioning, creation of dynamic library elements, plan layout and CAD data exchange, at least. Furthermore, the 
student should have experience in developing plug-ins for CAD-Tools, and at least basic knowledge of some of the used 
programming languages – e.g., Visual Basic for Applications (VBA) and c#.