---
slides: ""
url_pdf: ""
summary: ""
authors:
  - Manuel Wimmer
  - Tanja Mayerhofer
  - Bernhard Wally
  - Michael Schadler
  - Gertrude Kappel
url_video: ""
date: 2020-05-19 12:38:31+00:00
external_link: ""
url_slides: ""
title: CPS/IoT Ecosystem
tags:
  - Ongoing
image:
  caption: ""
  focal_point: ""
  preview_only: false
categories: []
url_code: ""
---
### Topic

[CPS/IoT Ecosystem](http://cpsiot.at/) is a joint initiative under „Hochschulraum-Strukturmittel (HRSM)“ national funding platform administered by Austrian Federal Ministry of Science, Research and Economy. Cyber-physical systems (CPS) is a concept that unifies all computer driven systems interacting closely with their physical environment. Internet-of-things (IoT) is a union of devices and technologies that provide universal interconnection mechanisms between physical and digital world. These two concepts stand in a close relation and in many cases are part of the same system. Both fields offer a number of interesting challenges from the industrial and scientific aspect.

The CPS/IoT Ecosystem project aims to bring these two fields closer together by evaluating intersection points in theory and in practice. The major goals of CPS/IoT Ecosystems are:

* Evaluate enabling state-of-the-art technologies for CPS/IoT
* Develop smart applications for buildings, mobility, farming, production

The CPS/IoT Ecosystem has five partners TU Wien, AIT, IST, TTTech and BMWFW as the founding agency. However, the project will indirectly include more institutions within Austria and EU. The goal is to provide interactive educational platform that would also include students and provide them with an opportunity to gain hands-on experience in CPS/IoT.

### Term

01.06.2017 - 31.05.2021

### Funding

CPS/IoT Ecosystem is funded by the Austrian Federal Ministry of Science, Research and Economy.