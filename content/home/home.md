---
advanced:
  css_style: ""
  css_class: ""
widget: blank
active: true
subtitle: ""
title: Business Informatics Group
weight: 15
headless: true
design:
  columns: "1"
  background: {}
  spacing:
    padding:
      - 20px
      - "0"
      - 20px
      - "0"
---
The Business Informatics Group (BIG) is a research division of the Institute of Information Systems Engineering at TU Wien. Given its name, the division focuses on business informatics that integrates theory and methods of information systems and computer science. In particular, BIG works on those information technology aspects that have a significant effect on the way organizations do their business. 

Thereby, BIG addresses the gap between the business strategy on why/what to do and the information technology aspect on how to do it in the digital age. The current research areas of BIG cover model-driven engineering, data engineering, process engineering, enterprise engineering, and industrial engineering.

{{< figure library="true" src="team.jpg" title="&copy; Stefanie Starz" lightbox="false" >}}